import random
import cv2 as cv
import imutils
import numpy as np
from skimage.util import random_noise


# Classe responsável por fazer transformações em um frame (rotacionar, espelhar, aplicar ruído ou aplicar escala
# de cinza.
class FrameOperator:

    # Método estático que recebe como parâmetro um frame, o nome do frame e um registro json, responsável por aplicar
    # as transformações necessárias no frame.
    @staticmethod
    def transform_image(frame, name, instructions):

        print('Creating...' + name)

        name = name + '('
        # Continua no loop enquanto ouver transformações a serem realizadas na imagem.
        for operation in instructions["op_type"].split("|"):

            # Rotaciona a imagem no sentido anti-horário em um ângulo aleatório de 0 a 180 graus.
            if operation == "random_rotation":
                angle = -random.random() * 180
                frame = imutils.rotate_bound(frame, angle)
                name = name + "rotation_"

            # Espelha a imagem horizontalmente.
            if operation == "flip":
                frame = cv.flip(frame, 1)
                name = name + "flip_"

            # Aplica um ruído na imagem.
            if operation == "noise":
                frame = random_noise(frame, mode="s&p", amount=0.3)
                frame = np.array(255 * frame, dtype="uint8")
                name = name + "noise_"

            # Aplica escala de cinza na imagem.
            if operation == "grayscale":
                frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
                name = name + "grayscale_"

        name = name + ').jpg'
        # Salva o frame.
        cv.imwrite(name, frame)
