import consumer
import producer

if __name__ == '__main__':

    # Chamamos a função que produz as mensagens e as manda pro RabbitMQ.
    producer.send_messages("payload.json")

    # Declaramos a classe consume e chamamos o método responsável por consumir as mensagens do RabbitMQ.
    receiver = consumer.Consume()
    receiver.consume_message()

