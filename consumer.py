import pika
import frame_extractor
import frame_operator


# Classe responsável por consumir as mensagens das filas do Rabbit.
class Consume:

    def __init__(self):

        # Se conecta com o Rabbit com a fila de que desejamos consumir as mensagens.
        self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue='augmentation_data')

        # Declaramos 2 objetos responsáveis por extrair o frame do vídeo e por fazer as modificações necessárias nele,
        # respectivamente.
        self.image_extractor = frame_extractor.FrameExtractor()
        self.image_op = frame_operator.FrameOperator()

    # Função responsável por consumir as mensagens da fila do Rabbit.
    def consume_message(self):

        i = 0
        # Enquanto dentro do loop, consome mensagens.
        while True:
            method, properties, body = self.channel.basic_get(queue='augmentation_data', auto_ack=True)

            # Caso não haja mais mensagens, ou caso já tenham sido consumidas 1500 mensagens, sai do loop.
            if method is None or i >= 1500:
                self.connection.close()
                break

            instructions = body.decode()
            print(" [x] Received %r" % instructions)

            # Extraímos o frame do video especificado na mensagem.
            self.image_extractor.store_instructions(instructions)
            ret, frame, name = self.image_extractor.get_frame()

            # Se o frame foi realmente extraído aplicamos nele as transformações especificadas na mensagem.
            if ret:
                self.image_op.transform_image(frame, name, self.image_extractor.get_instructions())

            # Caso contrário printamos que o frame não foi criado.
            else:
                print("Frame " + name + " not created.")
