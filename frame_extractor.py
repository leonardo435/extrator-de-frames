import cv2 as cv
import json


# Classe responsável pela extração de um frame especificado em um registro.
class FrameExtractor:

    # Indice da extração de frame sendo realizada.
    index = 1

    def __init__(self):
        self.instructions = []

    # Função responsável por armazenar uma instrução em json.
    def store_instructions(self, text_instructions):
        self.instructions = json.loads(text_instructions)

    # Função responsável por retornar a instrução json armazenada no objeto dessa classe.
    def get_instructions(self):
        return self.instructions

    # Função responsável por extrair o frame especificado na instrução.
    def get_frame(self):

        # Abrimos o arquivo do vídeo de que devemos extrair o frame.
        video = cv.VideoCapture("videos/" + self.instructions["video_ref"])

        # Se o vídeo foi aberto:
        if video.isOpened():

            # Seta o ponteiro do vídeo para o segundo do frame correto.
            video.set(propId=cv.CAP_PROP_POS_MSEC, value=(self.instructions["frame_seconds_index"] * 1000) + 400)

            # Cria um nome para o frame.
            name = './frames/frame(' + self.instructions["video_ref"] + ")(sec:" + str(
                self.instructions["frame_seconds_index"]) + ')(' + str(self.index) + ')'

            # Atualiza o índice da operação de extração da classe.
            self.index = self.index + 1

            # Lê o frame do vídeo.
            ret, frame = video.read()

            # Se o frame não foi lido, lê o último frame do vídeo.
            if not ret:
                currentframe = video.get(propId=cv.CAP_PROP_FRAME_COUNT) - 4
                video.set(propId=cv.CAP_PROP_POS_FRAMES, value=currentframe)
                ret, frame = video.read()

            # Fecha o vídeo.
            video.release()

            # Retorna se o vídeo foi lido, retorna o frame lido, e retorna o nome do frame.
            return ret, frame, name



