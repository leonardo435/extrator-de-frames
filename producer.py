import json
import pika


# Função que abre um arquivo json com o nome passado por parâmetro e envia cada registro como uma mensagem para o
# RabbitMQ.
def send_messages(json_name):
    # Conecta ao RabbitMQ e cria uma fila chamada augmentation_data.
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='augmentation_data')

    # Lê o arquivo json.
    json_file = open(json_name)
    json_data = json.load(json_file)

    # Fecha o arquivo json.
    json_file.close()

    # Em cada iteração manda uma mensagem para o Rabbit.
    for json_message in json_data:
        text_message = json.dumps(json_message)
        channel.basic_publish(exchange='',
                              routing_key='augmentation_data',
                              body=text_message)

        print('[x] Sent ' + text_message)

    # Fecha a conecção com o Rabbit.
    connection.close()
